class DARZedArmorInfo_DAR_Goliath extends DARZedArmorInfo_DAR;

defaultproperties
{
	ArmorZones.Add((ArmorZoneName=head,SocketName=FX_Armor_Head,ArmorHealth=250,ArmorHealthMax=250,ExplosionSFXTemplate=AkEvent'WW_ZED_Evil_DAR.Play_ZED_EvilDAR_SFX_Headshot')) //300
    ArmorZones.Add((ArmorZoneName=front,SocketName=FX_Armor_Chest,ArmorHealth=250,ArmorHealthMax=250,ExplosionSFXTemplate=AkEvent'WW_ZED_Evil_DAR.Play_ZED_EvilDAR_SFX_Jetpack_Damaged')) //300
}