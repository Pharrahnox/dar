class DARFlightHandler extends ReplicationInfo;

var DARPawn_ZedDAR DARPawn;
var DARAIController_ZedDAR DARController;

/*********************************************************************************************************
 * Flight - state variables
 *********************************************************************************************************/

/** Whether we are currently flying. */
var repnotify bool bFlying;

/** The z (height) value when flight was started. */
var float InitialFlightZ;

/** The time flight started. */
var float FlightStartTime;

/** The time we last performed an expensive check. */
var float LastStartFlightCheckTime, LastCancelFlightCheckTime, LastPauseFlightCheckTime, LastHoverCheckTime;

/** The last result from expensive checks. These values are used until the next expensive check occurs. */
var bool bLastStartFlightExpensiveResult, bLastCancelFlightExpensiveResult, bLastPauseFlightExpensiveResult,
	bLastHoverExpensiveResult;

/** If we haven't seen the enemy for this long, we cannot start flying. */
var float TimeSinceLastEnemySightingFlightTime;

/** If we haven't seen the enemy for this long, flight should be cancelled (mid-flight). */
var float TimeSinceLastEnemySightingFlightCancelTime;

/*********************************************************************************************************
 * Flight - AI settings
 *********************************************************************************************************/

/** Whether we can fly. */
var bool bCanFly;

/** Should we only fly if we have an enemy? */
var bool bOnlyFlyWithValidTarget;

/** Should we only fly near the enemy? */
var bool bOnlyFlyNearEnemy;

/** Whether we should set vertical speed to zero when initially pausing flight. */
var bool bZeroVelocityOnInitialPauseFlight;

/** Whether we should set vertical speed to zero when ending flight. */
var bool bZeroVelocityOnEndFlight;

/** If the player is closer than this, then we should consider cancelling flight. */
var float FlightMinDistanceToEnemy;

/**
  * Cancel or prevent flight if we're not within this distance of the enemy in the xy-plane. Only has an
  * affect if bOnlyFlyNearEnemy=true.
  */
var float FlightMaxDistanceToEnemy;

/**
  * Cancel flight if we're within this vertical distance of the enemy, and are within
  * FlightMinDistanceToEnemy in the xy-plane.
  */
var float FlightCancelEnemyHeightDiff;

/** Flight can only be cancelled due to proximity if we've been flying for at least this long. */
var float FlightCancelProximityTime;

/** Determines when it is appropriate to fly (i.e. we won't fly if the ceiling is lower than this). */
var float MinCeilingHeight;

/** The maximum height (in Unreal units, i.e. centimeters) when flying. Beyond this, flight is cancelled. */
var float MaxFlightHeight;

/** When hovering, maintain this vertical distance from the ceiling. */
var float HoverHeightFromCeiling;

/**
  * The time between checking each change to flight status. Used to limit expensive operations (e.g. traces)
  * which could cause performance issues if used every frame.
  */
var float StartFlightCheckInterval, CancelFlightCheckInterval, PauseFlightCheckInterval, HoverCheckInterval;

/*********************************************************************************************************
 * Flight - physics settings
 *********************************************************************************************************/
 
/** The current upwards thrust. */
var float UpwardsThrust;

/** Each time flight is initiated, the upwards thrust is chosen between these bounds. */
var float UpwardsThrustMin, UpwardsThrustMax;

/**
  * Each time flight is initiated, the vertical component of the velocity is immediately set to a value
  * between these bounds. This gives an initial vertical boost which is useful for giving extra height
  * during flight, particularly for high sprint speed and low upwards thrust.
  */
var float InitialBoostMin, InitialBoostMax;

/** The deceleration magnitude when transitioning from flying to hovering. */
var float HoverBrakeStrength;

/*********************************************************************************************************
 * Armour
 *********************************************************************************************************/

//TODO: move to the armour info class?

/**
  * The multiplier applied to the upwards thrust when body armour is still attached. Typically <= 1 to
  * simulate additional weight.
  */
var float ArmourThrustFactor;

/**
  * The multiplier applied to the vertical boost when body armour is still attached. Typically <= 1 to
  * simulate additional weight.
  */
var float ArmourBoostFactor;

replication
{
	if(bNetDirty)
		DARPawn, DARController, bFlying, UpwardsThrust, InitialFlightZ;
}

simulated event PostBeginPlay()
{
	Super.PostBeginPlay();
	
	DARPawn = DARPawn_ZedDAR(Owner);
}

simulated event Tick(float DeltaTime)
{
	Super.Tick(DeltaTime);
	
	if(bFlying)
	{
		TickFlight(DeltaTime);
	}
	else
	{
		//TODO: CheckStartFlight();
	}
}

/*********************************************************************************************************
 * Flight initiation and landing
 *********************************************************************************************************/

/** Starts or ends flight based on the new sprint status. */
function DetermineFlightFromSprint(bool bNewSprintStatus)
{
	SetFlying(bNewSprintStatus && ShouldStartFlight());
}

/** Handles changes to the flight status. */
function SetFlying(optional bool bFly = true)
{
	if(DARPawn.SpinOutHandler.bSpinningOut)
	{
		return;
	}
	
	if(bFly && !bFlying)
	{
		StartFlight();
	}
	else if(!bFly && bFlying)
	{
		EndFlight();
	}
}

/** Start flying. */
function StartFlight()
{
	//PHYS_Flying seems to disable the effect of gravity and prevents Velocity.Z from constantly being set
	//to zero when on the ground.
	DARPawn.SetPhysics(PHYS_Flying);
	
	bFlying = true;
	
	UpwardsThrust = GetUpwardsThrust();
	DARPawn.Velocity.Z = GetInitialBoost();
	
	InitialFlightZ = DARPawn.Location.Z;
	
	FlightStartTime = WorldInfo.TimeSeconds;
}

/** Stop flying. */
function EndFlight()
{
	DARPawn.SetPhysics(PHYS_Falling);
	
	bFlying = false;
	
	UpwardsThrust = 0.0;
	
	if(bZeroVelocityOnEndFlight)
	{
		DARPawn.Velocity.Z = 0;
	}
}

/** Returns the magnitude of the thrust to use for this flight. */
function float GetUpwardsThrust()
{
	local float Thrust;

	Thrust = Lerp(UpwardsThrustMin, UpwardsThrustMax, FRand());
	
	if(DARController.bHasBodyArmour)
	{
		Thrust *= ArmourThrustFactor;
	}
	
	return Thrust;
}

/** Returns the magnitude of the vertical boost at the start of this flight. */
function float GetInitialBoost()
{
	local float Boost;
	
	Boost = Lerp(InitialBoostMin, InitialBoostMax, FRand());
	
	if(DARController.bHasBodyArmour)
	{
		Boost *= ArmourBoostFactor;
	}
	
	return Boost;
}

/*********************************************************************************************************
 * Flight tick
 *********************************************************************************************************/

/** Called from Tick to handle switching flight status and flight motion. */
simulated function TickFlight(float DeltaTime)
{
	if(Role == ROLE_Authority && DARController != None)
	{
		if(ShouldCancelFlight())
		{
			EndFlight();
			return;
		}
		
		if(ShouldPauseFlight())
		{
			PauseFlight();
			return;
		}
		
		if(ShouldHover())
		{
			Hover();
			return;
		}
	}
	
	ContinueFlight(DeltaTime);
}

/** Pause flight, where we temporarily fall out of the air. */
function PauseFlight()
{
	if(DARPawn.Physics == PHYS_Flying)
	{
		DARPawn.SetPhysics(PHYS_Falling);
		
		if(bZeroVelocityOnInitialPauseFlight)
		{
			DARPawn.Velocity.Z = 0;
		}
	}
}

/** Handle hovering motion. */
simulated function Hover()
{
	if(DARPawn.Velocity.Z > 0)
	{
		//Slow down to vertical equilibrium.
		DARPawn.Velocity.Z = FMax(0, DARPawn.Velocity.Z - HoverBrakeStrength);
	}
}

/** Handle flight motion. */
simulated function ContinueFlight(float DeltaTime)
{
	//If we shouldn't pause the flight and we're not using flying physics, reset it. This should only happen
	//after unpausing flight.
	if(DARPawn.Physics != PHYS_Flying)
	{
		DARPawn.SetPhysics(PHYS_Flying);
	}
	
	ModifyThrust(DeltaTime);
	
	if(UpwardsThrust > 0.0)
	{
		DARPawn.Velocity.Z += UpwardsThrust * DeltaTime;
	}
	
	LimitXYVelocity();
}

/**
  * Can be overriden or filled to adjust thrust mid-flight. This could be used to achieve non-linear flight
  * or to adjust the flight speed based on other factors (e.g. distance to ground, ceiling, enemy, etc.).
  */
simulated function ModifyThrust(out float DeltaTime)
{
	//
}

simulated function LimitXYVelocity()
{
	local float OverflowScale;
	
	//Since AirSpeed doesn't seem to be limiting flying speed like it should, we manually limit velocity
	//when flying. We only want to limit velocity in the xy-plane.
	
	OverflowScale = VSize2D(DARPawn.Velocity) / DARPawn.AirSpeed;
	
	if(OverflowScale > 1.0)
	{
		DARPawn.Velocity.X /= OverflowScale;
		DARPawn.Velocity.Y /= OverflowScale;
	}
}

/*********************************************************************************************************
 * Flight status checks
 *********************************************************************************************************/

/*
	For each of the following checks for changes to flight status:
		-Simple checks are performed first. If they are sufficient, we are done.
		-If the simple checks aren't sufficient, we attempt to perform expensive checks.
		-If sufficient time has passed since the previous expensive check, we perform the expensive check.
			Otherwise we are done.
		-If the expensive checks pass, then the status change passes.
	
	Simple checks such as checking if references are valid or comparing times can be done each frame.
	Expensive checks such as traces should be used sparingly.
*/

function bool ShouldStartFlight()
{	
	if(!ShouldStartFlightSimple())
	{
		return false;
	}
	
	return ShouldStartFlightExpensive();
}

function bool ShouldCancelFlight()
{
	if(!ShouldCancelFlightSimple())
	{
		return false;
	}
	
	return ShouldCancelFlightExpensive();
}

function bool ShouldPauseFlight()
{
	if(!ShouldPauseFlightSimple())
	{
		return false;
	}
	
	return ShouldPauseFlightExpensive();
}

function bool ShouldHover()
{	
	if(!ShouldHoverSimple())
	{
		return false;
	}
	
	return ShouldHoverExpensive();
}

/*********************************************************************************************************
 * Flight status checks - simple
 *********************************************************************************************************/

function bool ShouldStartFlightSimple()
{
	//If we haven't seen the enemy for a while.
	if(`TimeSince(DARController.LastEnemySightedTime) > TimeSinceLastEnemySightingFlightTime)
	{
		return false;
	}
	
	//If we don't have a target.
	if(bOnlyFlyWithValidTarget && DARController.Enemy == None)
	{
		return false;
	}
	
	//If the enemy is too far away.
	if(bOnlyFlyNearEnemy && (DARController.Enemy == None || VSize(DARController.Enemy.Location -
		DARPawn.Location) > FlightMaxDistanceToEnemy))
	{
		return false;
	}
	
	return true;
}

function bool ShouldCancelFlightSimple()
{	
	//If we haven't seen an enemy for a while.
	if(`TimeSince(DARController.LastEnemySightedTime) > TimeSinceLastEnemySightingFlightCancelTime)
	{
		return true;
	}
	
	//If we've flown too high.
	if(DARPawn.Location.Z - InitialFlightZ > MaxFlightHeight)
	{
		return true;
	}
	
	//If we're moving towards a path node instead of the enemy.
	if(KFPawn(DARController.MoveTarget) == None)
	{
		return true;
	}
	
	//If we're too close to the enemy.
	if(CancelFlightFromEnemyProximity())
	{
		return true;
	}
	
	//If we're majorly afflicted.
	if(	DARPawn.IsDoingSpecialMove(DARPawn.ESpecialMove.SM_Frozen) ||
		DARPawn.IsDoingSpecialMove(DARPawn.ESpecialMove.SM_Knockdown) ||
		DARPawn.IsDoingSpecialMove(DARPawn.ESpecialMove.SM_Stunned) ||
		DARPawn.bFirePanicked || DARPawn.bEmpDisrupted || DARPawn.bEmpPanicked)
	{
		return true;
	}
	
	return false;
}

function bool ShouldPauseFlightSimple()
{
	if(DARPawn.IsDoingSpecialMove(DARPawn.ESpecialMove.SM_Stumble))
	{
		return true;
	}
	
	return false;
}

function bool ShouldHoverSimple()
{
	//Add non-expensive checks here.
	
	return true;
}

/** Returns whether flight should be cancelled due to proximity with the enemy. */
function bool CancelFlightFromEnemyProximity()
{
	local bool bSameHeight, bTooClose, bCanCancelFlight;
	
	bSameHeight = Abs(DARPawn.Location.Z - DARController.Enemy.Location.Z) < FlightCancelEnemyHeightDiff;
	bTooClose = VSize2D(DARPawn.Location - DARController.Enemy.Location) < FlightMinDistanceToEnemy;
	bCanCancelFlight = `TimeSince(FlightStartTime) > FlightCancelProximityTime;
	
	//All conditions must be satisfied simulataneously to cancel flight.
	return bSameHeight && bTooClose && bCanCancelFlight;
}

/*********************************************************************************************************
 * Flight status checks - expensive
 *********************************************************************************************************/

function bool ShouldStartFlightExpensive()
{	
	if(`TimeSince(LastStartFlightCheckTime) < StartFlightCheckInterval)
	{
		return bLastStartFlightExpensiveResult;
	}
	
	LastStartFlightCheckTime = WorldInfo.TimeSeconds;
	
	bLastStartFlightExpensiveResult = PerformExpensiveStartFlightChecks();
	
	return bLastStartFlightExpensiveResult;
}

function bool ShouldCancelFlightExpensive()
{
	if(`TimeSince(LastCancelFlightCheckTime) < CancelFlightCheckInterval)
	{
		return bLastCancelFlightExpensiveResult;
	}
	
	LastCancelFlightCheckTime = WorldInfo.TimeSeconds;
	
	bLastCancelFlightExpensiveResult = PerformExpensiveCancelFlightChecks();
	
	return bLastCancelFlightExpensiveResult;
}

function bool ShouldPauseFlightExpensive()
{
	if(`TimeSince(LastPauseFlightCheckTime) < PauseFlightCheckInterval)
	{
		return bLastPauseFlightExpensiveResult;
	}
	
	LastPauseFlightCheckTime = WorldInfo.TimeSeconds;
	
	bLastPauseFlightExpensiveResult = PerformExpensivePauseFlightChecks();
	
	return bLastPauseFlightExpensiveResult;
}

function bool ShouldHoverExpensive()
{
	if(`TimeSince(LastHoverCheckTime) < HoverCheckInterval)
	{
		return bLastHoverExpensiveResult;
	}
	
	LastHoverCheckTime = WorldInfo.TimeSeconds;
	
	bLastHoverExpensiveResult = PerformExpensiveHoverChecks();
	
	return bLastHoverExpensiveResult;
}

/**
  * Called from ShouldFlyExpensive. Do not call this directly otherwise some checks will be missed. Contains
  * expensive checks like traces.
  */
function bool PerformExpensiveStartFlightChecks()
{
	local Vector CeilingCheckOffset, HeadOffset;
	
	CeilingCheckOffset = vect(0,0,1) * MinCeilingHeight;
	HeadOffset = vect(0,0,1) * DARPawn.GetCollisionHeight();
	
	//If there are collisions directly above (e.g. a low ceiling).
	if(!FastTrace(DARPawn.Location + CeilingCheckOffset + HeadOffset, DARPawn.Location + HeadOffset,, true))
	{
		return false;
	}
	
	if(DARController.Enemy != None)
	{
		HeadOffset = vect(0,0,1) * DARController.Enemy.GetCollisionHeight();
		
		//If the enemy is indoors.
		if(!FastTrace(DARController.Enemy.Location + CeilingCheckOffset + HeadOffset,
			DARController.Enemy.Location + HeadOffset,, true))
		{
			return false;
		}
	}
	
	return true;
}

function bool PerformExpensiveCancelFlightChecks()
{
	//Add expensive checks here.
	
	return true;
}

function bool PerformExpensivePauseFlightChecks()
{
	//Add expensive checks here.
	
	return true;
}

function bool PerformExpensiveHoverChecks()
{
	local Vector CeilingCheckOffset, HeadOffset;
	
	LastHoverCheckTime = WorldInfo.TimeSeconds;
	
	CeilingCheckOffset = vect(0,0,1) * HoverHeightFromCeiling;
	HeadOffset = vect(0,0,1) * DARPawn.GetCollisionHeight();
	
	//If there are collisions directly above (e.g. the ceiling).
	if(!FastTrace(DARPawn.Location + CeilingCheckOffset + HeadOffset, DARPawn.Location + HeadOffset,, true))
	{
		return true;
	}
	
	return false;
}

defaultproperties
{
	//---------------------------------------------
	//Flight AI settings
	
	bCanFly=true
	bOnlyFlyWithValidTarget=true
	bOnlyFlyNearEnemy=true
	bZeroVelocityOnInitialPauseFlight=false
	bZeroVelocityOnEndFlight=false
	
	//Height checks
	MaxFlightHeight=1000.0
	MinCeilingHeight=300.0
	
	//Hover
	HoverHeightFromCeiling=150.0
	
	//AI check intervals
	StartFlightCheckInterval=1.0
	CancelFlightCheckInterval=0.2 //No traces currently, can do this more often.
	PauseFlightCheckInterval=0.2 //No traces currently, can do this more often.
	HoverCheckInterval=1.0
	
	TimeSinceLastEnemySightingFlightTime=2.0
	TimeSinceLastEnemySightingFlightCancelTime=4.0
	
	FlightCancelProximityTime=1.0
	FlightCancelEnemyHeightDiff=250.0
	FlightMinDistanceToEnemy=300.0
	FlightMaxDistanceToEnemy=2000.0
	
	//---------------------------------------------
	//Flight physics settings
	
	UpwardsThrustMin=650
	UpwardsThrustMax=1000
	
	InitialBoostMin=400
	InitialBoostMax=600
	
	HoverBrakeStrength=1000 //Deceleration (cm s^-2)
	
	//---------------------------------------------
	//Armour
	
	ArmourThrustFactor=0.65
	ArmourBoostFactor=0.65
}