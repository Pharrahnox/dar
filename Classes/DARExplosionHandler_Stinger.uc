class DARExplosionHandler_Stinger extends DARExplosionHandler;

defaultproperties
{
	//Large explosion light
	Begin Object Name=ExplosionPointLightLarge
		Radius=450
	End Object
	
	//Small explosion light
	Begin Object Name=ExplosionPointLightSmall
		Radius=300
	End Object
	
	//Large explosion
	Begin Object Name=ExploTemplate0
		Damage=10
		DamageRadius=250
		
		ExplosionScale=0.5
		
		CamShakeInnerRadius=200
		CamShakeOuterRadius=400
	End Object
	
	//Small explosion
	Begin Object Name=ExploTemplate0
		Damage=0
		DamageRadius=0
		
		ExplosionScale=0.18
		
		CamShakeInnerRadius=100
		CamShakeOuterRadius=250
	End Object
}