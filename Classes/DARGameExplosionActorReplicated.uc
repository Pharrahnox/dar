class DARGameExplosionActorReplicated extends KFExplosionActorReplicated
	notplaceable;

/** Store the ParticleSystemComponent returned from the EmitterPool so we can alter (e.g. scale) it. */
var ParticleSystemComponent PSComp;

simulated function SpawnExplosionParticleSystem(ParticleSystem Template)
{
	if(bSyncParticlesToMuzzle)
	{
		PSComp = WorldInfo.MyEmitterPool.SpawnEmitter(Template, GetMuzzleEffectLocation(), Rotation, None);
	}
	else
	{
		PSComp = WorldInfo.MyEmitterPool.SpawnEmitter(Template, Location, Rotation, None);
	}
}