class DARFlightHandler_Bomber extends DARFlightHandler;

defaultproperties
{
	UpwardsThrustMin=650
	UpwardsThrustMax=1000
	
	InitialBoostMin=400
	InitialBoostMax=600
	
	ArmourThrustFactor=0.65
	ArmourBoostFactor=0.65
	
	MaxFlightHeight=1000
}