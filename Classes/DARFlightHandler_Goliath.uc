class DARFlightHandler_Goliath extends DARFlightHandler;

defaultproperties
{
	UpwardsThrustMin=500
	UpwardsThrustMax=700
	
	InitialBoostMin=300
	InitialBoostMax=500
	
	ArmourThrustFactor=0.6
	ArmourBoostFactor=0.6
	
	MaxFlightHeight=800
}