class DARPawn_ZedDAR_Stinger extends DARPawn_ZedDAR;

var float MIC_Ice, MIC_DiffuseMult;
var bool bWasFrozen;

simulated event PostBeginPlay()
{
	Super.PostBeginPlay();
	
	//Apply the scale to the mesh.
	UpdateBodyScale(IntendedBodyScale);
	
	UpdateGameplayMICParams();
}

/** Applies the custom MIC parameters to the input MIC. */
simulated function ApplyCustomParamsToMIC(MaterialInstanceConstant MIC)
{
	if(MIC != None)
	{
		MIC.SetScalarParameterValue('Scalar_DiffuseMult', MIC_DiffuseMult);
		MIC.SetScalarParameterValue('Scalar_Ice', MIC_Ice);
	}
}

/** Overriden to cancel the attack mid-animation. */
simulated function StartRangedAttack()
{
	local KFSM_RangedAttack RangedSM;

	if(MyDARController != None)
	{
		RangedSM = KFSM_RangedAttack(SpecialMoves[SpecialMove]);
		
		if(RangedSM != None)
		{
			MyDARController.FireRangedAttack(RangedProjectileClass, RangedSM.GetFireOffset());
			
			//Cancel the attack so only one pair of rockets is fired.
			SetTimer(0.2 / RocketAttackRateMultiplier, false, nameof(EndSpecialMove));
		}
	}
}

defaultproperties
{
	MIC_Ice=0.4 //1.0
	MIC_DiffuseMult=0.6 //0.0
	
	RocketAttackRateMultiplier=1.0
	
	//Scales the size of the zed.
	IntendedBodyScale=0.7 //0.5
	
	Health=160 //130
	
	DoshValue=16 //12
	
	NameOverride="Stinger"
	
	ControllerClass=class'DARAIController_ZedDAR_Stinger'
	DifficultySettings=class'DARDifficulty_DAR_Stinger'
	FlightHandlerClass=class'DARFlightHandler_Stinger'
	SpinOutHandlerClass=class'DARSpinOutHandler_Stinger'
	ExplosionHandlerClass=class'DARExplosionHandler_Stinger'
	RangedProjectileClass=class'DARProj_DAR_RocketLight'
	ArmorInfoClass=class'DARZedArmorInfo_DAR_Stinger'
	
	// ---------------------------------------------
	// Movement / Physics
	GroundSpeed=180.0 //230
	SprintSpeed=500.0 //900
	AirSpeed=700.0
	
	Begin Object Name=MeleeHelper_0
		BaseDamage=6.0
		MaxHitRange=150.0 //180
		MomentumTransfer=15000.0 //250000
		MyDamageType=class'KFDT_Slashing_ZedWeak'
	End Object
	
	ParryResistance=1
	
	// for reference: Vulnerability=(default, head, legs, arms, special)
	IncapSettings(AF_Stun)=		(Vulnerability=(0.5, 2.0, 0.5, 0.5, 2.0), Cooldown=5.0,  Duration=1.5)
	IncapSettings(AF_Knockdown)=(Vulnerability=(0.3),                     Cooldown=5.0) //1.0; 1.0
	IncapSettings(AF_Stumble)=	(Vulnerability=(1.2),                     Cooldown=1.0) //1.0; 1.0
	IncapSettings(AF_GunHit)=	(Vulnerability=(1.0),                     Cooldown=0.5) //0.9; 0.2
	IncapSettings(AF_MeleeHit)=	(Vulnerability=(1.4),                     Cooldown=0.4) //1.0; 0.0
	IncapSettings(AF_FirePanic)=(Vulnerability=(1.0),                     Cooldown=6.0,  Duration=4.0) //0.5; 6.0; 5.0
	IncapSettings(AF_EMP)=		(Vulnerability=(2.5),                     Cooldown=5.0,  Duration=5.0)
	IncapSettings(AF_Poison)=	(Vulnerability=(0.05),	                  Cooldown=20.5, Duration=5.0)
	IncapSettings(AF_Microwave)=(Vulnerability=(3),                       Cooldown=6.5,  Duration=4.0)
	IncapSettings(AF_Freeze)=	(Vulnerability=(3.0),                     Cooldown=5.0,  Duration=3.5) //0.9; 1.0; 4.2
	IncapSettings(AF_Snare)=	(Vulnerability=(1.0, 1.0, 2.0, 1.0, 1.0), Cooldown=5.5,  Duration=3.0)
    IncapSettings(AF_Bleed)=    (Vulnerability=(0.01))
	
	Begin Object Name=SpecialMoveHandler_0
		SpecialMoveClasses(SM_Frozen)=class'DARSM_Frozen'
	End Object
	
	// Custom Hit Zones (HeadHealth, SkinTypes, etc...)
    HitZones[HZI_HEAD]=(ZoneName=head, BoneName=Head, Limb=BP_Head, GoreHealth=50, DmgScale=1.1, SkinID=1)
    HitZones[3]=(ZoneName=heart, BoneName=Spine1, Limb=BP_Special, GoreHealth=50, DmgScale=3.5, SkinID=2) //1.5
}