class DARSpinOutHandler extends ReplicationInfo;

var DARPawn_ZedDAR DARPawn;
var DARAIController_ZedDAR DARController;

/*
	When we spin out, three things happen:
		-We are flung in a direction (mostly upwards) at high speed.
		-We spin (rotate) around constantly.
		-We become very susceptible to damage.
	
	Behind the scenes, all AI commands are stopped (i.e. we do nothing by spin and fly out of control).
	
	Spinning out lasts until:
		-The spin out duration is exceeded, or
		-We are killed (after the safe duration), or
		-We hit a blocking object (after the safe duration).
	
	When spinning out stops, we explode if possible, otherwise we simply suicide.
*/

/*
	A note on values with variance:
	The actual value ranges between [AverageValue - Variance, AverageValue + Variance], with a uniform
	distribution of probability.
	
	For example, the flight speed when spinning out has:
		-A minimum value of SpinOutFlightSpeed - SpinOutFlightSpeedVariance, and
		-A maximum value of SpinOutFlightSpeed + SpinOutFlightSpeedVariance.
*/

/*********************************************************************************************************
 * Flight - state variables
 *********************************************************************************************************/

/** Whether we are currently spinning out. */
var repnotify bool bSpinningOut;

/** The time we started spinning out. */
var float SpinOutTime;

/** The Pawn's velocity in the previous frame. Used to monitor changes in velocity. */
var vector PrevVelocity;

/** The desired rotation when spinning out. This is constantly updated to force rotation. */
var rotator DesiredSpinOutRotation;

/*********************************************************************************************************
 * Flight - AI settings
 *********************************************************************************************************/

/** Whether we can spin out. */
var bool bCanSpinOut;

/** The average duration for spinning out. After this, spinning out is stopped. */
var float SpinOutDuration;

/** The variance in duration for spinning out. */
var float SpinOutDurationVariance;

/** The duration after starting to spin out for which we are safe from fully exploding. */
var float SpinOutSafeDuration;

/** The modifier for received damage to apply when spinning out. */
var float SpinOutDamageModifier;

/*********************************************************************************************************
 * Flight - physics settings
 *********************************************************************************************************/

/** The speed for spinning out. */
var float SpinOutFlightSpeed;

/** The variance in speed for spinning out. */
var float SpinOutFlightSpeedVariance;

/** The rotation rate magnitude for spinning out. */
var float SpinOutRotationRateMagnitude;

/** The variance in rotation rate magnitude for spinning out. */
var float SpinOutRotationRateMagnitudeVariance;

/** The rotation rate used when spinning. Set from SpinOut, specifically in InitSpinOutRotation. */
var rotator SpinOutRotationRate;

replication
{
	if(bNetDirty)
		DARPawn, DARController, bSpinningOut, SpinOutRotationRate;
}

simulated event PostBeginPlay()
{
	Super.PostBeginPlay();
	
	DARPawn = DARPawn_ZedDAR(Owner);
}

simulated event Tick(float DeltaTime)
{
	Super.Tick(DeltaTime);
	
	if(bSpinningOut)
	{
		TickSpinOut(DeltaTime);
	}
}

/*********************************************************************************************************
 * Spin out
 *********************************************************************************************************/

/** Called from Tick to handle rotation during spin out and checking if spin out should be stopped. */
simulated function TickSpinOut(float DeltaTime)
{
	if(Role == ROLE_Authority && ShouldStopSpinOut())
	{
		StopSpinOut();
		return;
	}
	
	PrevVelocity = DARPawn.Velocity;
	
	DesiredSpinOutRotation += SpinOutRotationRate * DeltaTime;
	DARPawn.SetDesiredRotation(DesiredSpinOutRotation);
}

function bool ShouldSpinOut()
{
	return bCanSpinOut && !bSpinningOut && DARPawn.Health > 0;
}

/** Returns whether spin out should be stopped. */
function bool ShouldStopSpinOut()
{
	if(VSize(DARPawn.Velocity) < 0.01)
	{
		return true;
	}
	
	//Unfortunately the collision events (e.g. HitWall) don't seem to fire. Instead we check for changes in
	//velocity, particularly the direction to determine if we've hit a wall. The method used here is merely
	//a guess, but seems to work well.
	return VSize(DARPawn.Velocity - PrevVelocity) / VSize(DARPawn.Velocity) > 0.1;
}

/**
  * Called from SpinOut to handle clearing AICommands, setting up the Pawn for spinning out, etc. Don't call
  * this directly.
  */
function PreSpinOut()
{
	DARPawn.SetSprinting(false);
	
	//Call this before setting bSpinningOut=true otherwise it will have no effect.
	//Stop controlled flight if we're currently flying.
	DARPawn.FlightHandler.SetFlying(false);
	
	DARPawn.EndSpecialMove();
	
	//Pop all AI commands as we don't want the DAR to do anything but spin out.
	//Do this before setting velocity, as it zeros the velocity.
	PopCommands();
	
	//Note that we set bFlying to false, but we want use the flying physics otherwise we can't fly off
	//during the spin out.
	DARPawn.SetPhysics(PHYS_Flying);
	
	//Prevents restrictions on flight or spin-out velocity.
	DARPawn.AirSpeed = 10000.0;
	
	//Set the spinning out flag.
	bSpinningOut = true;
	//Store the start time of spinning out.
	SpinOutTime = WorldInfo.TimeSeconds;
}

/** Causes the DAR to spin out. There's no coming back from this (currently). */
function SpinOut()
{	
	PreSpinOut();
	
	InitSpinOutRotation();
	InitSpinOutVelocity();
	
	SetTimer(GetSpinOutDuration(), false, nameof(StopSpinOut));
}

function InitSpinOutRotation()
{
	//We use the DesiredSpinOutRotation as our desired rotation. We constantly change the
	//DesiredSpinOutRotation according to the SpinOutRotationRate to force the DAR to spin around. This is
	//required because a lot of AI code seems to try to override the desired rotation, even with no
	//AICommands on the stack.
	DesiredSpinOutRotation = DARPawn.Rotation;
	DARPawn.SetDesiredRotation(DesiredSpinOutRotation);
	
	//Create a random rotator which will be used to continuously spin the DAR.
	SpinOutRotationRate = GetSpinOutRotationRate();
}

function InitSpinOutVelocity()
{
	//Start flying, and store the velocity. We monitor changes in velocity to determine if we've hit a wall.
	DARPawn.Velocity = GetSpinOutFlightVelocity();
	PrevVelocity = DARPawn.Velocity;
}

function vector GetSpinOutFlightVelocity()
{
	local vector SpinOutFlightVelocity;
	
	SpinOutFlightVelocity.X = 1.0 - 2.0*FRand(); //Ranges between (-1,1).
	SpinOutFlightVelocity.Y = 1.0 - 2.0*FRand(); //Ranges between (-1,1).
	SpinOutFlightVelocity.Z = 1.0; //We want to fly upwards mostly.
	SpinOutFlightVelocity = Normal(SpinOutFlightVelocity) * GetSpinOutFlightSpeed();
	
	return SpinOutFlightVelocity;
}

function rotator GetSpinOutRotationRate()
{
	local rotator BaseRotator;
	local float CurrentRotationRate, DesiredRotationRate;
	
	BaseRotator = GetRandomRotator();
	
	//This is the effective rotation rate of the random rotator (using 3D Euclidean distance).
	//Need to cast to float to avoid overflow (and therefore negative then imaginary numbers).
	CurrentRotationRate = Sqrt(float(BaseRotator.Pitch) * BaseRotator.Pitch + float(BaseRotator.Yaw) *
		BaseRotator.Yaw + float(BaseRotator.Roll) * BaseRotator.Roll);
	
	//This is the rotation rate we desire.
	DesiredRotationRate = GetSpinOutRotationRateMagnitude();
	
	//Now we scale the rotator to give the rotation rate we desire.
	BaseRotator *= DesiredRotationRate / CurrentRotationRate;
	
	return BaseRotator;
}

/** Gets a random rotator with component values between 0 and 65535 - important for Pawn.Rotation. */
function rotator GetRandomRotator()
{
	local rotator RandomRotator;
	
	//Get a uniformly distributed random rotator. We don't use RotRand because the component values exceed
	//65535 and cause issues when added to Pawn.Rotation (which has component values ranging between
	//[0,65535]).
	/*BaseRotator.Pitch = 65535 * FRand();
	BaseRotator.Yaw = 65535 * FRand();
	BaseRotator.Roll = 65535 * FRand();*/
	
	//We don't use the previous system for generating a random rotator anymore. Instead we keep Roll=0
	//and couple pitch and roll so that they add to 65535, with both Pitch and Yaw non-negligible.
	RandomRotator.Pitch = 65535 * (0.2 + 0.6*FRand());
	RandomRotator.Yaw = 65535 - RandomRotator.Pitch;
	
	return RandomRotator;
}

function float GetSpinOutFlightSpeed()
{
	return ChooseFromUniformDistribution(SpinOutFlightSpeed, SpinOutFlightSpeedVariance);
}

function float GetSpinOutRotationRateMagnitude()
{
	return ChooseFromUniformDistribution(SpinOutRotationRateMagnitude, SpinOutRotationRateMagnitudeVariance);
}

function float GetSpinOutDuration()
{
	return ChooseFromUniformDistribution(SpinOutDuration, SpinOutDurationVariance);
}

//TODO: move to utility class.
function float ChooseFromUniformDistribution(float AverageValue, float Variance)
{
	return AverageValue + (1.0 - 2.0*FRand()) * Variance;
}

/** Pops all AI commands. */
function PopCommands()
{
	local GameAICommand Cmd;
	
	if(DARController == None)
	{
		return;
	}
	
	for(Cmd = DARController.CommandList; Cmd != None; Cmd = Cmd.ChildCommand)
	{
		DARController.PopCommand(Cmd);
	}
}

/** Stop spinning out. Currently, we come to a messy and permanent stop. */
function StopSpinOut()
{
	bSpinningOut = false;
	
	if(DARPawn != None)
	{
		if(DARPawn.ExplosionHandler.CanExplode())
		{
			DARPawn.ExplosionHandler.Explode();
		}
		else
		{
			DARPawn.Suicide();
		}
	}
}

defaultproperties
{
	bCanSpinOut=true
	
	SpinOutFlightSpeed=1000
	SpinOutFlightSpeedVariance=200
	
	SpinOutRotationRateMagnitude=20000
	SpinOutRotationRateMagnitudeVariance=5000
	
	SpinOutDuration=2.5
	SpinOutDurationVariance=0.5
	
	SpinOutSafeDuration=0.1
	
	SpinOutDamageModifier=10.0
}