class DARProj_DAR_RocketLight extends DARProj_DAR_Rocket;

defaultproperties
{
	Speed=2600 //3000
	
	Begin Object Name=ExploTemplate0
		Damage=9 //10
		DamageRadius=250 //300
		DamageFalloffExponent=1.5
		DamageDelay=0.0

		// Damage Effects
		MyDamageType=class'KFDT_EvilDAR_Rocket'
		MomentumTransferScale=20000.0 //60000 //30000
		KnockDownStrength=20 //100
	End Object
}