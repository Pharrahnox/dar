class DARSM_Frozen extends KFSM_Frozen;

function SetFrozenParameter(float FreezeAmount)
{
	Super.SetFrozenParameter(FreezeAmount);
	
	if(KFPOwner.WorldInfo.NetMode == NM_DedicatedServer)
	{
		return;
	}
	
	//Fully frozen.
	if(FreezeAmount == 1.0)
	{
		//Leave the MIC parameters as is.
	}
	//Fully thawed.
	else if(FreezeAmount == 0.0)
	{
		if(DARPawn_ZedDAR(KFPOwner) != None)
		{
			//Reapply the custom MIC parameters.
			DARPawn_ZedDAR(KFPOwner).ApplyCustomMICParams();
		}
	}
}

