class DARAIController_ZedDAR_Goliath extends DARAIController_ZedDAR;

defaultproperties
{
	RangeAttackIntervalNormal=9.0 //7.0
	RangeAttackIntervalHard=8.0 //6.0
	RangeAttackIntervalSuicidal=7.5 //5.5
	RangeAttackIntervalHellOnEarth=6.5 //5.0
	
	//TODO: make this dependent on difficulty.
	BarrageCountMax=2
}