class DARExplosionHandler extends Object
	within DARPawn_ZedDAR;

//NOTE: There is no need for Role == ROLE_Authority checks since this object only exists on the server.

/** Explosion template for backpack/suicide */
var DARGameExplosion SpinOutExplosionTemplateLarge;
var DARGameExplosion SpinOutExplosionTemplateSmall;

/** Set when we (semi-)explodes up to make sure it only happens once. */
var bool bHasSpinOutExploded;
var bool bHasSpinOutSemiExploded;

/** The mutliplier for self damage from the semi-explosion. */
var float SemiExplosionSelfDamageMultiplier;

/** Whether we should explode on the killing blow (e.g. if an impact decaps and kills us). */
var bool bCanSpinOutSemiExplodeOnKillingBlow;

/** Whether we can explode. */
var bool bCanSpinOutExplode;

/** Whether we can semi-explode. */
var bool bCanSpinOutSemiExplode;

function bool CanExplode()
{
	return bCanSpinOutExplode && !bHasSpinOutExploded && `TimeSince(SpinOutHandler.SpinOutTime) >=
		SpinOutHandler.SpinOutSafeDuration;
}

function bool CanSemiExplode()
{
	return bCanSpinOutSemiExplode && !bHasSpinOutSemiExploded && (Health > 0 ||
		bCanSpinOutSemiExplodeOnKillingBlow);
}

/*********************************************************************************************************
 * Explosion events
 *********************************************************************************************************/

/** Creates a large explosion - designed to destroy us. */
function Explode()
{
	local float ExplosionSelfDamage;
	
	bHasSpinOutExploded = true;
	
	ExplosionSelfDamage = 10000;
	ModifyExplosionSelfDamage(SpinOutExplosionTemplateLarge, ExplosionSelfDamage);
	
	TriggerExplosion(SpinOutExplosionTemplateLarge, Location, ExplosionSelfDamage);
}

/** Creates a small explosion - not designed to destroy us. */
function SemiExplode()
{
	local vector ExplosionLocation;
	local AkBaseSoundObject ExplosionSound;
	local float ExplosionSelfDamage;
	
	bHasSpinOutSemiExploded = true;
	
	ExplosionSelfDamage = SpinOutExplosionTemplateSmall.Damage * SemiExplosionSelfDamageMultiplier;
	ModifyExplosionSelfDamage(SpinOutExplosionTemplateSmall, ExplosionSelfDamage);
	
	//Right explosion.
	FiringSocketName = FiringSocketRName;
	ExplosionLocation = (Health > 0 && DARController != None) ? DARController.GetStartFireLocation() :
		Location;
	TriggerExplosion(SpinOutExplosionTemplateSmall, ExplosionLocation, ExplosionSelfDamage);
	
	//Store the explosion sound so we can reset it.
	ExplosionSound = SpinOutExplosionTemplateSmall.ExplosionSound;
	
	//Disable sound - we don't want to play two explosion sounds, it's too loud.
	SpinOutExplosionTemplateSmall.ExplosionSound = None;
	
	//Left explosion.
	FiringSocketName = FiringSocketLName;
	ExplosionLocation = (Health > 0 && DARController != None) ? DARController.GetStartFireLocation() :
		Location;
	TriggerExplosion(SpinOutExplosionTemplateSmall, ExplosionLocation, ExplosionSelfDamage);
	
	//Reset the explosion sound for the template.
	SpinOutExplosionTemplateSmall.ExplosionSound = ExplosionSound;
}

function ModifyExplosionSelfDamage(DARGameExplosion SpinOutExplosionTemplate, out float SelfDamage)
{
	//Can be overridden to modify the self damage of explosions. Ideally this would include GameInfo and
	//Mutator hooks.
}

/** Called from Explode or SemiExplode. Handles creation of the explosion, particle effects and damage. */
function TriggerExplosion(DARGameExplosion SpinOutExplosionTemplate, vector ExplosionLocation, float
	SelfDamage)
{
	local KFExplosionActorReplicated ExploActor;
	local Controller DamageInstigator;
	
	// explode using the given template
	//Use our custom explosion actor so we can access the particle system created.
	ExploActor = Spawn(class'DARGameExplosionActorReplicated', Outer);
	
	if(ExploActor != None)
	{
		DamageInstigator = (LastHitBy != None && KFPlayerController(LastHitBy) != None) ? LastHitBy
			: MyKFAIC;
		
		ExploActor.InstigatorController = DamageInstigator;
		ExploActor.Instigator = Outer;

		// Force ourselves to get hit.  These settings are not replicated,
		// but they only really make a difference on the server anyway.
		ExploActor.Attachee = Outer;

		ExploActor.Explode(SpinOutExplosionTemplate, vect(0,0,1));
		
		if(DARGameExplosionActorReplicated(ExploActor) != None)
		{
			AdjustExplosionParticleSystem(DARGameExplosionActorReplicated(ExploActor).PSComp,
				SpinOutExplosionTemplate);
		}
	}

	TakeRadiusDamage(DamageInstigator, SelfDamage, SpinOutExplosionTemplate.DamageRadius,
		SpinOutExplosionTemplate.MyDamageType, SpinOutExplosionTemplate.MomentumTransferScale,
		ExplosionLocation, true, Outer);
}

/** Modifies the particle system created by the explosion. Can be used to scale the particle effects. */
function AdjustExplosionParticleSystem(ParticleSystemComponent PSComp, DARGameExplosion
	SpinOutExplosionTemplate)
{
	if(PSComp == None || SpinOutExplosionTemplate == None)
	{
		return;
	}
	
	PSComp.SetScale(SpinOutExplosionTemplate.ExplosionScale);
}

/*********************************************************************************************************
 * Notification events
 *********************************************************************************************************/

/** Notification event fired when the Pawn takes damage. */
event NotifyTakeDamage(int Damage, Controller InstigatedBy, vector HitLocation, vector Momentum,
	class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
	//If we've just been killed and we were spinning out.
	if(TimeOfDeath == WorldInfo.TimeSeconds && SpinOutHandler.bSpinningOut && CanExplode())
	{
		Explode();
	}
}

/** Notification event fired when a hit zone takes damage. */
function NotifyTakeHitZoneDamage(float Damage, class<DamageType> DamageType, int HitZoneIdx, vector
	InstigatorLocation)
{
	//
}

/** Notification event fired when a hit zone is completely destroyed (e.g. decapitation for the head). */
function NotifyHitZoneInjured(optional int HitZoneIdx=INDEX_None)
{
	if(HitZoneIdx == HZI_Head)
	{
		if(CanSemiExplode())
		{
			SemiExplode();
		}
		
		if(SpinOutHandler.ShouldSpinOut())
		{
			SpinOutHandler.SpinOut();
		}
	}
}

defaultproperties
{
	bCanSpinOutExplode=true
	bCanSpinOutSemiExplode=true
	bCanSpinOutSemiExplodeOnKillingBlow=false
	
	SemiExplosionSelfDamageMultiplier=0.0
	
	//Large explosion light
	Begin Object Class=PointLightComponent Name=ExplosionPointLightLarge
	    LightColor=(R=245,G=190,B=140,A=255)
		bCastPerObjectShadows=false
		Radius=600
		FalloffExponent=2.0
	End Object
	
	//Small explosion light
	Begin Object Class=PointLightComponent Name=ExplosionPointLightSmall
	    LightColor=(R=245,G=190,B=140,A=255)
		bCastPerObjectShadows=false
		Radius=400
		FalloffExponent=2.0
	End Object

	//Large explosion
	Begin Object Class=DARGameExplosion Name=ExploTemplate0
		Damage=30
		DamageRadius=500
		DamageFalloffExponent=1.0
		DamageDelay=0.0
		bFullDamageToAttachee=true
		
		ExplosionScale=1.0

		// Damage Effects
		MyDamageType=class'KFDT_Explosive_HuskSuicide'
		FractureMeshRadius=200.0
		FracturePartVel=500.0
		ExplosionEffects=KFImpactEffectInfo'FX_Impacts_ARCH.Explosions.HuskSuicide_Explosion'
		ExplosionSound=AkEvent'WW_ZED_Husk.ZED_Husk_SFX_Suicide_Explode'
		MomentumTransferScale=1.0

		// Dynamic Light
        ExploLight=ExplosionPointLightLarge
        ExploLightStartFadeOutTime=0.0
        ExploLightFadeOutTime=0.5

		// Camera Shake
		CamShake=CameraShake'FX_CameraShake_Arch.Misc_Explosions.HuskSuicide'
		CamShakeInnerRadius=300
		CamShakeOuterRadius=700
		CamShakeFalloff=1.0
		bOrientCameraShakeTowardsEpicenter=true
	End Object
	
	//Small explosion
	Begin Object Class=DARGameExplosion Name=ExploTemplate1
		Damage=15
		DamageRadius=300
		DamageFalloffExponent=1.5
		DamageDelay=0.f
		bFullDamageToAttachee=true

		ExplosionScale=0.3
		
		// Damage Effects
		MyDamageType=class'KFDT_Explosive_HuskSuicide'
		FractureMeshRadius=200.0
		FracturePartVel=500.0
		ExplosionEffects=KFImpactEffectInfo'FX_Impacts_ARCH.Explosions.HuskSuicide_Explosion'
		ExplosionSound=AkEvent'WW_WEP_EXP_Dynamite.Play_WEP_EXP_Dynamite_Explosion'
		MomentumTransferScale=1.0

		// Dynamic Light
        ExploLight=ExplosionPointLightSmall
        ExploLightStartFadeOutTime=0.0
        ExploLightFadeOutTime=0.5

		// Camera Shake
		CamShake=CameraShake'FX_CameraShake_Arch.Misc_Explosions.HuskSuicide'
		CamShakeInnerRadius=200
		CamShakeOuterRadius=450
		CamShakeFalloff=2.0
		bOrientCameraShakeTowardsEpicenter=true
	End Object
	
	SpinOutExplosionTemplateLarge=ExploTemplate0
	SpinOutExplosionTemplateSmall=ExploTemplate1
}