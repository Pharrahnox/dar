class DARExplosionHandler_Bomber extends DARExplosionHandler;

defaultproperties
{
	//Large explosion
	Begin Object Name=ExploTemplate0
		Damage=25
		DamageRadius=400
	End Object
	
	//Small explosion
	Begin Object Name=ExploTemplate0
		Damage=10
		DamageRadius=250
	End Object
}