class DARAIController_ZedDAR_Stinger extends DARAIController_ZedDAR;

defaultproperties
{
	RangeAttackIntervalNormal=8.0 //5.0
	RangeAttackIntervalHard=7.0 //4.0
	RangeAttackIntervalSuicidal=6.0 //3.5
	RangeAttackIntervalHellOnEarth=5.0 //3.0
	
	BarrageCountMax=1
}