class DARGameInfo_Endless extends KFGameInfo_Endless;

/** The classes for the flying DARs. */
var class<KFPawn_Monster> StingerClass, BomberClass, GoliathClass;

/** The chance for overriding an appropriate zed with each flying DAR type. */
var float StingerOverrideChance, BomberOverrideChance, GoliathOverrideChance;

/** Overridden to replace some zeds with flying DARs. */
function class<KFPawn_Monster> GetAISpawnType(EAIType AIType)
{
	local class<KFPawn_Monster> OverrideClass;
	
	OverrideClass = GetOverrideClass(AIType);
	
    if(OverrideClass != None)
	{
		return OverrideClass;
	}
	else
	{
		return Super.GetAISpawnType(AIType);
	}
}

function class<KFPawn_Monster> GetOverrideClass(EAIType AIType)
{
	local class<KFPawn_Monster> OverrideClass;
	
	if(ShouldSpawnFlyingDARs())
	{
		OverrideClass = GetFlyingDAROverride(AIType);
		
		if(OverrideClass != None)
		{
			return OverrideClass;
		}
	}
	
	//Add other checks here for other override types.
	
	return None;
}

function bool ShouldSpawnFlyingDARs()
{
	//Don't spawn flying DARs on the first few waves.
	return MyKFGRI.WaveNum > 3;
}

function class<KFPawn_Monster> GetFlyingDAROverride(EAIType AIType)
{
	switch(AIType)
	{
		case AT_Clot:
		case AT_SlasherClot:
		case AT_Crawler:
		case AT_Gorefast:
		case AT_Stalker:
			return (FRand() < StingerOverrideChance) ? StingerClass : None;
		
		case AT_Bloat:
		case AT_Siren:
		case AT_Husk:
			return (FRand() < BomberOverrideChance) ? BomberClass : None;
		
		case AT_FleshpoundMini:
		case AT_Fleshpound:
		case AT_Scrake:
			return (FRand() < GoliathOverrideChance) ? GoliathClass : None;
		
		default:
			return None;
	}
}

defaultproperties
{
	//Replace the player controller to use a different cheat manager which allows spawning our custom DARs.
	PlayerControllerClass=class'DARPlayerController'
	
	//Replace this so we GUI manager so we can fix custom zed names in the post game report.
	KFGFxManagerClass=class'DARGFxMoviePlayer_Manager'
	
	//Replace these so that they can't (elite) spawn the default DARs.
	AIClassList(AT_Stalker)=class'DARPawn_ZedStalker'
	AIClassList(AT_Husk)=class'DARPawn_ZedHusk'
	
	StingerClass=class'DARPawn_ZedDAR_Stinger'
	BomberClass=class'DARPawn_ZedDAR_Bomber'
	GoliathClass=class'DARPawn_ZedDAR_Goliath'
	
	StingerOverrideChance=0.07
	BomberOverrideChance=0.17
	GoliathOverrideChance=0.28
}