class DARProj_DAR_Rocket extends KFProj_EvilDAR_Rocket;

defaultproperties
{
	Speed=1900 //2000
	
	Begin Object Name=ExploTemplate0
		Damage=7.0 //9
		DamageRadius=250 //275
		DamageFalloffExponent=1.5
		DamageDelay=0.0

		// Damage Effects
		MyDamageType=class'KFDT_EvilDAR_Rocket'
		MomentumTransferScale=10000.0 //60000 //30000
		KnockDownStrength=20 //100
	End Object
}