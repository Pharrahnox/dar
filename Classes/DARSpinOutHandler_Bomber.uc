class DARSpinOutHandler_Bomber extends DARSpinOutHandler;

defaultproperties
{
	SpinOutFlightSpeed=2700
	SpinOutFlightSpeedVariance=500
	
	SpinOutRotationRateMagnitude=90000
	SpinOutRotationRateMagnitudeVariance=30000
	
	SpinOutDuration=1.8
	SpinOutDurationVariance=0.5
}