class DARAIController_ZedDAR extends KFAIController_ZedDAR_Rocket;

var DARPawn_ZedDAR DARPawn;

/*********************************************************************************************************
 * Rocket barrage
 *********************************************************************************************************/

/** The amount of rocket barrages left in the current chain. */
var int BarrageCount;

/** The number of rocket barrages in a chain. */
var int BarrageCountMax;

/*********************************************************************************************************
 * Armour
 *********************************************************************************************************/

/** Whether the DAR currently has specific armour pieces still attached. */
var bool bHasBodyArmour, bHasHelmet;

replication
{
	if(bNetDirty)
		DARPawn, bHasBodyArmour, bHasHelmet;
}

event Possess(Pawn InPawn, bool bVehicleTransition)
{
	Super.Possess(InPawn, bVehicleTransition);
	
	DARPawn = DARPawn_ZedDAR(Pawn);
}

/** Overridden to store changes in armour status. */
function OnArmorLoss(name ArmorName)
{
	MyKFPawn.AfflictionHandler.AccrueAffliction(AF_Stumble, ArmorLossStumblePower);

	if(ArmorName == 'head')
	{
		bHasHelmet = false;
		
		BaseTimeBetweenRangeAttacks += HeadLossAttackTimeDebuff;
		AimError *= (1.f + HeadLossAccuracyDebuffPct);
	}
	else if(ArmorName == 'front')
	{
		//Don't disable sprint when the body armour is broken.
		/*bCanSprint = false;
		bAllowedToSprint = false;
		bCanEvade = false;
		MyKFPawn.bIsSprinting = false;*/
		
		bHasBodyArmour = false;
	}
}

/*********************************************************************************************************
 * Rocket barrage
 *********************************************************************************************************/

/** Overriden to perform Enemy != None check. */
function FireRangedAttack(class<KFProjectile> RangedProjectileClass, vector ProjectileOffset)
{
	if(Enemy == None)
	{
		return;
	}
	
	Super.FireRangedAttack(RangedProjectileClass, ProjectileOffset);
}

/** Overriden to send shooting notification and handle chained rocket barrages. */
function StartRangedAttack()
{
	Super.StartRangedAttack();
	
	NotifyShooting();
	
	if(BarrageCount == 0)
	{
		BarrageCount = BarrageCountMax;
	}
}

/** A notification event for shooting. */
simulated function NotifyShooting()
{
	DARPawn.FlightHandler.UpwardsThrust = 0.0;
}

/** Ends the chain of rocket barrages if it should be discontinued. */
simulated function EndBarrage()
{
	DecrementBarrageCount();
	
	if(!CanContinueBarrage())
	{
		ClearTimer(nameof(EndBarrage));
		
		BarrageCount = 0;
		MyKFPawn.EndSpecialMove();
	}
}

/** Whether the current rocket barrage chain can be continued. */
simulated function bool CanContinueBarrage()
{
	return BarrageCount > 0 && CanSee(Enemy);
}

simulated function DecrementBarrageCount()
{
	BarrageCount--;
}

function bool CanDoRangedAttack(float DistanceToTargetSqr)
{
	if(DARPawn.SpinOutHandler.bSpinningOut)
	{
		return false;
	}
	
	return Super.CanDoRangedAttack(DistanceToTargetSqr);
}

defaultproperties
{
	bCanEvade=false
	
	//---------------------------------------------
	//Armour
	
	bHasBodyArmour=true
	bHasHelmet=true
	
	//---------------------------------------------
	//Rocket barrage
	
	BarrageCountMax = 1;
}