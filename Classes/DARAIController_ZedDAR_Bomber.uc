class DARAIController_ZedDAR_Bomber extends DARAIController_ZedDAR;

defaultproperties
{
	RangeAttackIntervalNormal=9.0 //6.0
	RangeAttackIntervalHard=8.0 //5.0
	RangeAttackIntervalSuicidal=7.0 //4.5
	RangeAttackIntervalHellOnEarth=6.0 //4.0
	
	BarrageCountMax=1
}