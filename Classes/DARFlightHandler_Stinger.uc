class DARFlightHandler_Stinger extends DARFlightHandler;

defaultproperties
{
	UpwardsThrustMin=900
	UpwardsThrustMax=1400
	
	InitialBoostMin=600
	InitialBoostMax=900
	
	ArmourThrustFactor=0.7
	ArmourBoostFactor=0.7
	
	MaxFlightHeight=1200
}