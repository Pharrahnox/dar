class DARDifficulty_DAR_Goliath extends DARDifficulty_DAR;

defaultproperties
{
	// Body and head health scale by number of players
	NumPlayersScale_BodyHealth=0.2 //0.39 for scrake and fleshpound, 0.2 for quarterpound
	NumPlayersScale_HeadHealth=0.15 //0.28 for scrake and fleshpound, 0.15 for quarterpound
}