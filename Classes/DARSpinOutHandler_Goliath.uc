class DARSpinOutHandler_Goliath extends DARSpinOutHandler;

defaultproperties
{
	SpinOutFlightSpeed=2000
	SpinOutFlightSpeedVariance=400
	
	SpinOutRotationRateMagnitude=50000
	SpinOutRotationRateMagnitudeVariance=15000
	
	SpinOutDuration=2.3
	SpinOutDurationVariance=0.5
}