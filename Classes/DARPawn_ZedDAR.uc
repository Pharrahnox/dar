class DARPawn_ZedDAR extends KFPawn_ZedDAR_Rocket
	abstract;

var DARAIController_ZedDAR DARController;

/** Multiplies the fire rate of the rocket barrage. */
var float RocketAttackRateMultiplier;

/** If the DAR's health percentage is below this when decapitated, it should be killed. */
var float HeadlessKillHealthPercentage;

/** Health is divided by this factor when decapitated. */
var float HeadlessHealthReductionFactor;

/** The override name for this zed. */
var string NameOverride;

var DARFlightHandler FlightHandler;
var DARSpinOutHandler SpinOutHandler;
var DARExplosionHandler ExplosionHandler;

var class<DARFlightHandler> FlightHandlerClass;
var class<DARSpinOutHandler> SpinOutHandlerClass;
var class<DARExplosionHandler> ExplosionHandlerClass;

replication
{
	//Don't worry about replicating the headless variables and the explosion handler as they're only used
	//server-side. Damage and explosions are handled on the server and replicated to the client already.
	if(bNetDirty)
		DARController, FlightHandler, SpinOutHandler, RocketAttackRateMultiplier;
}

simulated event PostBeginPlay()
{
	Super.PostBeginPlay();
	
	CreateHandlers();
}

simulated function CreateHandlers()
{
	if(Role == ROLE_Authority)
	{
		CreateFlightHandler();
		CreateSpinOutHandler();
		CreateExplosionHandler();
	}
}

function CreateFlightHandler()
{
	//Set ourself as the owner.
	FlightHandler = Spawn(FlightHandlerClass, self);
}

function CreateSpinOutHandler()
{
	//Set ourself as the owner.
	SpinOutHandler = Spawn(SpinOutHandlerClass, self);
}

function CreateExplosionHandler()
{
	ExplosionHandler = new(self) ExplosionHandlerClass;
}

function PossessedBy(Controller C, bool bVehicleTransition)
{
	Super.PossessedBy(C, bVehicleTransition);
	
	DARController = DARAIController_ZedDAR(Controller);
	FlightHandler.DARController = DARController;
	SpinOutHandler.DARController = DARController;
}

simulated function UpdateGameplayMICParams()
{
    Super.UpdateGameplayMICParams();

	ApplyCustomMICParams();
}

/** Alters the material properties of the DAR. */
simulated function ApplyCustomMICParams()
{
	local MaterialInstanceConstant MIC;
	local int i;
	
	//We don't care about visuals on a dedicated server.
	if(WorldInfo.NetMode == NM_DedicatedServer)
	{
		return;
	}
	
	//The selection of these lists was inspired by KFSM_Frozen.SetFrozenParamater.
	
	foreach CharacterMICs(MIC)
	{
		ApplyCustomParamsToMIC(MIC);
	}
	
	for(i = 0; i < `MAX_COSMETIC_ATTACHMENTS; ++i)
	{
		ApplyCustomParamsToMesh(ThirdPersonAttachments[i]);
	}
	
	for(i = 0; i < StaticAttachList.Length; ++i)
	{
		ApplyCustomParamsToMesh(StaticAttachList[i]);
	}
}

simulated function ApplyCustomParamsToMesh(MeshComponent MeshToAlter)
{
	local MaterialInstanceConstant MIC;
	local int i;
	
	if(MeshToAlter != None)
	{
		for(i = 0; i < MeshToAlter.Materials.Length; ++i)
		{
			MIC = MaterialInstanceConstant(MeshToAlter.GetMaterial(i));
			
			ApplyCustomParamsToMIC(MIC);
		}
	}
}

/** Applies the custom MIC parameters to the input MIC. */
simulated function ApplyCustomParamsToMIC(MaterialInstanceConstant MIC);

function SetSprinting(bool bNewSprintStatus)
{
	if(SpinOutHandler.bSpinningOut)
	{
		return;
	}
	
	Super.SetSprinting(bNewSprintStatus);
	
	//Check if we should change flight status based on the sprint status request.
	//Note that we use bNewSprintStatus not bIsSprinting because DARs seem to start the StartSprint
	//animation based on bNewSprintStatus - even if bIsSprinting is false.
	FlightHandler.DetermineFlightFromSprint(bNewSprintStatus);
}

/** Overridden to add RangedSM != None check. */
simulated function StartRangedAttack()
{
	local KFSM_RangedAttack RangedSM;

	if(DARController != None)
	{
		RangedSM = KFSM_RangedAttack(SpecialMoves[SpecialMove]);
		
		if(RangedSM != None)
		{
			DARController.FireRangedAttack(RangedProjectileClass, RangedSM.GetFireOffset());
		}
	}
}

/** Called when we are decapitated. */
function CauseHeadTrauma(optional float BleedOutTime = 5.f)
{
	//Reduce health when decapitated.
	
	if(Health / float(HealthMax) < HeadlessKillHealthPercentage)
	{
		Super(KFPawn_Monster).CauseHeadTrauma(HeadlessBleedOutTime);
	}
	else
	{
		Health /= HeadlessHealthReductionFactor;
	}
}

event TakeDamage(int Damage, Controller InstigatedBy, vector HitLocation, vector Momentum, class<DamageType>
	DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
	ModifyDamageReceived(Damage, InstigatedBy, HitLocation, Momentum, DamageType, HitInfo, DamageCauser);
	
	Super.TakeDamage(Damage, InstigatedBy, HitLocation, Momentum, DamageType, HitInfo, DamageCauser);
	
	//We don't have an explosion handler on the client - explosions are replicated.
	if(Role == ROLE_Authority && !bPlayedDeath && ExplosionHandler != None)
	{
		ExplosionHandler.NotifyTakeDamage(Damage, InstigatedBy, HitLocation, Momentum, DamageType, HitInfo,
			DamageCauser);
	}
}

/** Potentially modifies incoming damage. */
function ModifyDamageReceived(out int Damage, Controller InstigatedBy, vector HitLocation, vector Momentum,
	class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{	
	if(Damage > 0 && SpinOutHandler != None && SpinOutHandler.bSpinningOut)
	{
		Damage *= SpinOutHandler.SpinOutDamageModifier;
	}
}

/** Apply damage to a specific zone (useful for gore effects) */
function TakeHitZoneDamage(float Damage, class<DamageType> DamageType, int HitZoneIdx, vector
	InstigatorLocation)
{
	Super.TakeHitZoneDamage(Damage, DamageType, HitZoneIdx, InstigatorLocation);
	
	//We don't have an explosion handler on the client - explosions are replicated.
	if(Role == ROLE_Authority && !bPlayedDeath && ExplosionHandler != None)
	{
		ExplosionHandler.NotifyTakeHitZoneDamage(Damage, DamageType, HitZoneIdx, InstigatorLocation);
	}
}

/** Reliably play any gore effects related to a zone/limb being dismembered */
simulated function HitZoneInjured(optional int HitZoneIdx=INDEX_None)
{
	Super.HitZoneInjured(HitZoneIdx);
	
	//We don't have an explosion handler on the client - explosions are replicated.
	if(Role == ROLE_Authority && !bPlayedDeath && ExplosionHandler != None)
	{
		ExplosionHandler.NotifyHitZoneInjured(HitZoneIdx);
	}
}

/** Overridden to prevent special moves when spinning out. */
simulated event bool CanDoSpecialMove(ESpecialMove AMove, optional bool bForceCheck)
{
	if(!bPlayedDeath && SpinOutHandler != None && SpinOutHandler.bSpinningOut)
	{
		return false;
	}
	
	return Super.CanDoSpecialMove(AMove, bForceCheck);
}

simulated function PlayDying(class<DamageType> DamageType, vector HitLoc)
{
	if(FlightHandler != None)
	{
		//IMPORTANT: set bFlying to false otherwise the corpse hangs in the air on death mid-flight.
		FlightHandler.SetFlying(false);
	}
	
	Super.PlayDying(DamageType, HitLoc);
	
	SetTimer(0.01, false, nameof(CleanupHandlers));
}

/** Destroys handlers and references to them. */
simulated function CleanupHandlers()
{
	if(FlightHandler != None)
	{
		FlightHandler.Destroy();
		FlightHandler = None;
	}
	
	if(SpinOutHandler != None)
	{
		SpinOutHandler.Destroy();
		SpinOutHandler = None;
	}
	
	ExplosionHandler = None;
}

static function string GetLocalizedName()
{
	return default.NameOverride;
}

defaultproperties
{
	//Hack to prevent the default health reduction on decapitation. See KFPawn_Monster.AdjustDamage.
	bCheckingExtraHeadDamage=true
	
	RocketAttackRateMultiplier=1.0
	
	HeadlessKillHealthPercentage=0.0 //0.2
	HeadlessHealthReductionFactor=1.0 //2.5
	HeadlessBleedOutTime=2.0
	
	NameOverride="Flying E.D.A.R."
	
	ControllerClass=class'DARAIController_ZedDAR'
	FlightHandlerClass=class'DARFlightHandler'
	SpinOutHandlerClass=class'DARSpinOutHandler'
	ExplosionHandlerClass=class'DARExplosionHandler'
	
	Begin Object Name=SpecialMoveHandler_0
		SpecialMoveClasses(SM_StandAndShootAttack)=class'DARSM_DAR_RocketAttack'
	End Object
}