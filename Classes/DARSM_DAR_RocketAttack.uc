class DARSM_DAR_RocketAttack extends KFSM_DAR_RocketAttack;

var DARPawn_ZedDAR DARPawn;
var DARAIController_ZedDAR DARController;

/** Overridden to apply a custom animation rate and to send the EndBarrage notification event. */
function PlayAnimation()
{
	local int AnimNum;
	local float AnimRate;
	local float Duration;
	
	if(DARPawn == None)
	{
		DARPawn = DARPawn_ZedDAR(KFPOwner);
	}
	
	if(DARController == None)
	{
		DARController = DARAIController_ZedDAR(AIOwner);
	}
	
	//Use the custom animation rate if possible.
	AnimRate = (DARPawn != None) ? DARPawn.RocketAttackRateMultiplier : 1.0;
	
	//Stuff from Super.
	AnimNum = Clamp(KFPOwner.SpecialMoveFlags, 0, AnimNames.Length);
	AnimName = AnimNames[AnimNum];
	FireOffset = FireOffsets[AnimNum];
	
	Duration = PlaySpecialMoveAnim(AnimName, AnimStance, BlendInTime, BlendOutTime, AnimRate, bLoopAnim);
	
	if(DARController != None)
	{
		//Tell the controller when the animation is going to be finished. This is used to determine when
		//the looping animation should be discontinued.
		DARController.SetTimer(Duration, true, nameof(DARController.EndBarrage));
	}
}

defaultproperties
{
	bLoopAnim=true
}