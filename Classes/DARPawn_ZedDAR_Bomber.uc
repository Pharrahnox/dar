class DARPawn_ZedDAR_Bomber extends DARPawn_ZedDAR;

defaultproperties
{
	RocketAttackRateMultiplier=3.0 //5.0
	
	Health=500 //450
	
	DoshValue=25 //18
	
	NameOverride="Bomber"
	
	ControllerClass=class'DARAIController_ZedDAR_Bomber'
	DifficultySettings=class'DARDifficulty_DAR_Bomber'
	FlightHandlerClass=class'DARFlightHandler_Bomber'
	SpinOutHandlerClass=class'DARSpinOutHandler_Bomber'
	ExplosionHandlerClass=class'DARExplosionHandler_Bomber'
	RangedProjectileClass=class'DARProj_DAR_Rocket'
	ArmorInfoClass=class'DARZedArmorInfo_DAR_Bomber'
	
	// ---------------------------------------------
	// Movement / Physics
	GroundSpeed=170.0 //180
	SprintSpeed=400.0 //600
	AirSpeed=600.0
	
	Begin Object Name=MeleeHelper_0
		BaseDamage=9.0
		MaxHitRange=160.0 //180
		MomentumTransfer=20000.0 //250000
		MyDamageType=class'KFDT_Slashing_ZedWeak'
	End Object
	
	ParryResistance=2 //3
	
	// for reference: Vulnerability=(default, head, legs, arms, special)
	IncapSettings(AF_Stun)=		(Vulnerability=(0.5, 2.0, 0.5, 0.5, 2.0), Cooldown=5.0,  Duration=1.5)
	IncapSettings(AF_Knockdown)=(Vulnerability=(0.2),                     Cooldown=5.0) //1.0; 1.0
	IncapSettings(AF_Stumble)=	(Vulnerability=(0.5),                     Cooldown=2.0) //1.0; 1.0
	IncapSettings(AF_GunHit)=	(Vulnerability=(0.6),                     Cooldown=1.5) //0.9; 0.2
	IncapSettings(AF_MeleeHit)=	(Vulnerability=(0.5),                     Cooldown=0.6) //1.0; 0.0
	IncapSettings(AF_FirePanic)=(Vulnerability=(0.3),                     Cooldown=8.0,  Duration=3.5) //0.5; 6.0; 5.0
	IncapSettings(AF_EMP)=		(Vulnerability=(2.5),                     Cooldown=5.0,  Duration=5.0)
	IncapSettings(AF_Poison)=	(Vulnerability=(0.05),	                  Cooldown=20.5, Duration=5.0)
	IncapSettings(AF_Microwave)=(Vulnerability=(3),                       Cooldown=6.5,  Duration=4.0)
	IncapSettings(AF_Freeze)=	(Vulnerability=(1.5),                     Cooldown=5.0,  Duration=3.5) //0.9; 1.0; 4.2
	IncapSettings(AF_Snare)=	(Vulnerability=(1.0, 1.0, 2.0, 1.0, 1.0), Cooldown=5.5,  Duration=3.0)
    IncapSettings(AF_Bleed)=    (Vulnerability=(0.01))
	
	// Custom Hit Zones (HeadHealth, SkinTypes, etc...)
    HitZones[HZI_HEAD]=(ZoneName=head, BoneName=Head, Limb=BP_Head, GoreHealth=150, DmgScale=1.001, SkinID=1) //180
    HitZones[3]=(ZoneName=heart, BoneName=Spine1, Limb=BP_Special, GoreHealth=150, DmgScale=3.5, SkinID=2)
}