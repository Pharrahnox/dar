class DARGFxPostGameContainer_PlayerStats extends KFGFxPostGameContainer_PlayerStats;

/**
  * This function is responsible for displaying the amount of each zed type that you killed in the
  * post game report (in the Player Stats tab).
  */
function GFxObject MakeZedKillObject(class<KFPawn_Monster> MonsterClass, string SecondaryText)
{
	local GFxObject TempObject;
	local string MonsterName;
	
	if(class<DARPawn_ZedDAR>(MonsterClass) != None)
	{
		MonsterName = class<DARPawn_ZedDAR>(MonsterClass).static.GetLocalizedName();
	}
	else
	{
		//TWI uses MonsterClass.Name instead of LocalizationKey. Not sure why, since this is what the
		//localisation key is for... We use the key so that our equivalent zeds have correct localisation.
		MonsterName = Localize("Zeds", String(MonsterClass.default.LocalizationKey), "KFGame");
	}
	
	TempObject = CreateObject("Object");

	TempObject.SetString("typeString", "ZED_KILL");
	TempObject.SetString("title", MonsterName);
	TempObject.SetString("value", SecondaryText);
	TempObject.SetBool("bSkipAnim", true);
	ItemCount++;

	return TempObject;
}