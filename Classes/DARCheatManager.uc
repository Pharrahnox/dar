class DARCheatManager extends KFCheatManager;

/** Get a zed class from the name. */
function class<KFPawn_Monster> LoadMonsterByName(string ZedName, optional bool bIsVersusPawn)
{
    local class<KFPawn_Monster> SpawnClass;

	if(Left(ZedName, 5) ~= "ClotA")
	{
		SpawnClass = class<KFPawn_Monster>(DynamicLoadObject("DAR.DARPawn_ZedClot_Alpha", class'Class'));
	}
	else if(Left(ZedName, 3) ~= "Sta")
	{
		SpawnClass = class<KFPawn_Monster>(DynamicLoadObject("DAR.DARPawn_ZedStalker", class'Class'));
	}
    else if(Left(ZedName, 2) ~= "Hu")
    {
        SpawnClass = class<KFPawn_Monster>(DynamicLoadObject("DAR.DARPawn_ZedHusk", class'Class'));
    }
    else if(Left(ZedName, 2) ~= "Bo")
    {
        SpawnClass = class<KFPawn_Monster>(DynamicLoadObject("DAR.DARPawn_ZedDAR_Bomber", class'Class'));
    }
    else if(Left(ZedName, 3) ~= "Sti")
    {
        SpawnClass = class<KFPawn_Monster>(DynamicLoadObject("DAR.DARPawn_ZedDAR_Stinger", class'Class'));
    }
    else if(Left(ZedName, 3) ~= "Gol")
    {
        SpawnClass = class<KFPawn_Monster>(DynamicLoadObject("DAR.DARPawn_ZedDAR_Goliath", class'Class'));
    }
    else
    {
        SpawnClass = Super.LoadMonsterByName(ZedName, bIsVersusPawn);
    }

    return SpawnClass;
}