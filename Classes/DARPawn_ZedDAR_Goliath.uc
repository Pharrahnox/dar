class DARPawn_ZedDAR_Goliath extends DARPawn_ZedDAR;

var float MIC_Burnt, MIC_BurntGlowContrast, MIC_DiffuseMult;

simulated event PostBeginPlay()
{
	Super.PostBeginPlay();
	
	//Apply the scale to the mesh.
	UpdateBodyScale(IntendedBodyScale);
	
	UpdateGameplayMICParams();
}

/** Applies the custom MIC parameters to the input MIC. */
simulated function ApplyCustomParamsToMIC(MaterialInstanceConstant MIC)
{
	if(MIC != None)
	{
		MIC.SetScalarParameterValue('Scalar_DiffuseMult', MIC_DiffuseMult);
		MIC.SetScalarParameterValue('Scalar_Burnt', MIC_Burnt);
		MIC.SetScalarParameterValue('Scalar_BurntGlowContrast', MIC_BurntGlowContrast);
	}
}

defaultproperties
{
	MIC_Burnt=0.7 //1.0
	MIC_BurntGlowContrast=1.5 //2.0
	MIC_DiffuseMult=0.5 //0.4
	
	//Scales the size of the zed.
	IntendedBodyScale=1.35 //1.5
	
	RocketAttackRateMultiplier=3.0
	
	Health=1000 //1200
	
	DoshValue=60 //90
	
	NameOverride="Goliath"
	
	ControllerClass=class'DARAIController_ZedDAR_Goliath'
	DifficultySettings=class'DARDifficulty_DAR_Goliath'
	FlightHandlerClass=class'DARFlightHandler_Goliath'
	SpinOutHandlerClass=class'DARSpinOutHandler_Goliath'
	ExplosionHandlerClass=class'DARExplosionHandler_Goliath'
	RangedProjectileClass=class'DARProj_DAR_RocketHeavy'
	ArmorInfoClass=class'DARZedArmorInfo_DAR_Goliath'
	
	// ---------------------------------------------
	// Movement / Physics
	GroundSpeed=220.0 //180
	SprintSpeed=400.0 //300
	AirSpeed=500.0
	
	Begin Object Name=MeleeHelper_0
		BaseDamage=13.0 //15.0
		MaxHitRange=180.0
		MomentumTransfer=25000.0
		MyDamageType=class'KFDT_Slashing_ZedWeak'
	End Object
	
	ParryResistance=3 //4
	
	// for reference: Vulnerability=(default, head, legs, arms, special)
	IncapSettings(AF_Stun)=		(Vulnerability=(0.5, 2.0, 0.5, 0.5, 2.0), Cooldown=13.0,  Duration=1.5) //0.5, _; 5.0; 1.5
	IncapSettings(AF_Knockdown)=(Vulnerability=(0.1),                     Cooldown=10.0) //1.0; 1.0
	IncapSettings(AF_Stumble)=	(Vulnerability=(0.35),                    Cooldown=7.0) //1.0; 1.0
	IncapSettings(AF_GunHit)=	(Vulnerability=(0.4),                     Cooldown=1.8) //0.9; 0.2
	IncapSettings(AF_MeleeHit)=	(Vulnerability=(0.4),                     Cooldown=1.2) //1.0; 0.0
	IncapSettings(AF_FirePanic)=(Vulnerability=(0.2),                     Cooldown=12.0,  Duration=3.0) //0.5; 6.0; 5.0
	IncapSettings(AF_EMP)=		(Vulnerability=(2.0),                     Cooldown=10.0,  Duration=3.5) //2.0; 5.0; 5.0
	IncapSettings(AF_Poison)=	(Vulnerability=(0.05),	                  Cooldown=20.5, Duration=5.0)
	IncapSettings(AF_Microwave)=(Vulnerability=(3),                       Cooldown=10.0,  Duration=3.0) //3.0; 6.5; 4.0
	IncapSettings(AF_Freeze)=	(Vulnerability=(1.2),                     Cooldown=12.0,  Duration=2.5) //0.9; 1.0; 4.2
	IncapSettings(AF_Snare)=	(Vulnerability=(1.0, 1.0, 2.0, 1.0, 1.0), Cooldown=5.5,  Duration=3.0)
    IncapSettings(AF_Bleed)=    (Vulnerability=(0.01))
	
	Begin Object Name=Afflictions_0
		//Set these values high enough so that the charring effect is never applied.
		//We don't want it applied because we already set the burn effect by default.
		FireFullyCharredDuration=100.0
   	 	FireCharPercentThreshhold=1.1
	End Object
	
	// Custom Hit Zones (HeadHealth, SkinTypes, etc...)
    HitZones[HZI_HEAD]=(ZoneName=head, BoneName=Head, Limb=BP_Head, GoreHealth=350, DmgScale=1.1, SkinID=1) //500
    HitZones[3]=(ZoneName=heart, BoneName=Spine1, Limb=BP_Special, GoreHealth=400, DmgScale=3.5, SkinID=2)
}