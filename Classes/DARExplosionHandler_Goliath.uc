class DARExplosionHandler_Goliath extends DARExplosionHandler;

defaultproperties
{
	//Large explosion light
	Begin Object Name=ExplosionPointLightLarge
		Radius=800
	End Object
	
	//Small explosion light
	Begin Object Name=ExplosionPointLightSmall
		Radius=500
	End Object
	
	//Large explosion
	Begin Object Name=ExploTemplate0
		Damage=40
		DamageRadius=600
		
		ExplosionScale=1.5
		
		CamShakeInnerRadius=400
		CamShakeOuterRadius=800
	End Object
	
	//Small explosion
	Begin Object Name=ExploTemplate0
		Damage=20
		DamageRadius=250
		
		ExplosionScale=1.3
		
		CamShakeInnerRadius=300
		CamShakeOuterRadius=550
	End Object
}