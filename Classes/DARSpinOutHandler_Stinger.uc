class DARSpinOutHandler_Stinger extends DARSpinOutHandler;

defaultproperties
{
	SpinOutFlightSpeed=3000
	SpinOutFlightSpeedVariance=600
	
	SpinOutRotationRateMagnitude=120000
	SpinOutRotationRateMagnitudeVariance=40000
	
	SpinOutDuration=1.5
	SpinOutDurationVariance=0.3
}