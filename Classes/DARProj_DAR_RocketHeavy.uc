class DARProj_DAR_RocketHeavy extends DARProj_DAR_Rocket;

defaultproperties
{
	Speed=1500 //1700
	
	DrawScale=2.0
	
	Begin Object Name=ExploTemplate0
		Damage=10 //13
		DamageRadius=300 //325
		DamageFalloffExponent=1.5
		DamageDelay=0.0

		// Damage Effects
		MyDamageType=class'KFDT_EvilDAR_Rocket'
		MomentumTransferScale=20000.0 //60000 //30000
		KnockDownStrength=35 //100
	End Object
}