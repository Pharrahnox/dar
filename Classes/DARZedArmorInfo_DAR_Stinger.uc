class DARZedArmorInfo_DAR_Stinger extends DARZedArmorInfo_DAR;

defaultproperties
{
	ArmorZones.Add((ArmorZoneName=head,SocketName=FX_Armor_Head,ArmorHealth=40,ArmorHealthMax=40,ExplosionSFXTemplate=AkEvent'WW_ZED_Evil_DAR.Play_ZED_EvilDAR_SFX_Headshot')) //20
    ArmorZones.Add((ArmorZoneName=front,SocketName=FX_Armor_Chest,ArmorHealth=60,ArmorHealthMax=60,ExplosionSFXTemplate=AkEvent'WW_ZED_Evil_DAR.Play_ZED_EvilDAR_SFX_Jetpack_Damaged')) //40
}