class DARGameExplosion extends KFGameExplosion;

/** The scale of the explosion particle system. */
var float ExplosionScale;

defaultproperties
{
	ExplosionScale=1.0
}